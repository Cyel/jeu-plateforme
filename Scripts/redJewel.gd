extends Sprite

onready var ScriptJoueur = get_tree().get_root().find_node("Joueur", true, false)
onready var ScriptGUI = get_tree().get_root().find_node("GUI", true, false)
onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)

func _on_Area2D_body_entered(_body):
	ScriptJoueur.ramassageArgent()
	ScriptStats.argent += 1 
	ScriptStats.argentPourVie += 1
	ScriptGUI.change_val_argent(ScriptStats.argent)
	queue_free()
	if ScriptStats.argentPourVie >= 5 : 
		ScriptStats.argentPourVie -= 5
		ScriptStats.forme += 1
		if ScriptStats.forme > 3 :
			ScriptStats.forme = 3
			ScriptStats.vie += 1
			ScriptGUI.change_val_vie(ScriptStats.vie)
		ScriptGUI.change_val_argent(ScriptStats.argent)
		ScriptGUI.etat_du_joueur(ScriptStats.forme)
