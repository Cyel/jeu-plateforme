extends KinematicBody2D

onready var ScriptGUI = get_tree().get_root().find_node("GUI", true, false)
onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)

var velocite = Vector2(150,0)

func _ready():
	var _tailleDEcran = get_viewport_rect().size
 
func _on_Area2D_body_entered(body):
	if body.is_in_group("Joueur"):#La forme de collision enfant de l'Area2D a été rajouté dans le groupe ennemi, le joueur a été ajouté dans le groupe "Joueur".
		if ScriptStats.forme > 0 :
			ScriptStats.forme -= 1
		ScriptGUI.etat_du_joueur(ScriptStats.forme)


func _physics_process(delta):
	var collision = move_and_collide(velocite * delta)
	if collision:
		velocite.x = - velocite.x#.bounce(collision.normal)
		velocite.y=0
