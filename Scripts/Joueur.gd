extends KinematicBody2D

#Initialisation variables : 
#Autre scène
onready var ScriptGUI = get_tree().get_root().find_node("GUI", true, false)
onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)
const VITESSE = 500 #donnee à 700 pixels/secondes
const GRAVITE = 2000#donnée à 2000
const FORCE_DU_SAUT = -700 #donnée à -1000
#Par défaut, Godot travaille avec une vue du dessus. 
#Ce jeu nécessite une vue de côté, on fixe donc la direction du haut. 
#Cela est aussi possible en utilisant la constante prédéfini par godot : Vector2.UP
const DIRECTION_DU_HAUT = Vector2(0,-1) 
#Déplacement du personnage (direction)
var direction = Vector2()



var son_argent
var son_saut
var son_mort

func _ready():
	son_argent = load("res://Assets1/Sounds/coin1.wav")
	son_saut = load("res://Assets1/Sounds/jump_01.wav")
	son_mort = load("res://Assets1/Sounds/hjm-tesla_sound_shot.wav")

#Tourne en boucle : comme process mais plus spécifique au KinematicBody.
func _physics_process(delta):
	marche()#Appel de la fonction de même nom. 
	gravite(delta)
	saut()
	#Appliquer le mouvement
	move_and_slide(direction, DIRECTION_DU_HAUT)




func marche():
	if ScriptStats.forme == 3 :
		if Input.is_action_pressed("ui_right"):
			direction.x = VITESSE
			$AnimatedSprite.play("Marche3")
			$AnimatedSprite.flip_h = false
		elif Input.is_action_pressed("ui_left"):
			direction.x = -VITESSE
			$AnimatedSprite.play("Marche3")
			$AnimatedSprite.flip_h = true
		else:
			direction.x = 0
			$AnimatedSprite.play("Attente3")
	elif ScriptStats.forme == 2 :
		if Input.is_action_pressed("ui_right"):
			direction.x = VITESSE
			$AnimatedSprite.play("Marche2")
			$AnimatedSprite.flip_h = false
		elif Input.is_action_pressed("ui_left"):
			direction.x = -VITESSE
			$AnimatedSprite.play("Marche2")
			$AnimatedSprite.flip_h = true
		else:
			direction.x = 0
			$AnimatedSprite.play("Attente2")
	elif ScriptStats.forme == 1 :
		if Input.is_action_pressed("ui_right"):
			direction.x = VITESSE
			$AnimatedSprite.play("Marche1")
			$AnimatedSprite.flip_h = false
		elif Input.is_action_pressed("ui_left"):
			direction.x = -VITESSE
			$AnimatedSprite.play("Marche1")
			$AnimatedSprite.flip_h = true
		else:
			direction.x = 0
			$AnimatedSprite.play("Attente1")
	elif ScriptStats.forme <= 0 :
		$AnimatedSprite.play("Mort")
	if Input.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://Scenes/Menu.tscn")



func gravite(delta):
	if is_on_floor():#Si on touche le sol on ne tombe plus
		direction.y=0
	else: #Sinon on tombe
		direction.y += GRAVITE * delta


func saut():
	if is_on_floor() and Input.is_action_pressed("jump"):
		direction.y = FORCE_DU_SAUT
		$AudioStreamPlayer2D.stream = son_saut
		$AudioStreamPlayer2D.play()
	if direction.y <0:
		if ScriptStats.forme == 3 :
			$AnimatedSprite.play("Saut3")
		elif ScriptStats.forme == 2 :
			$AnimatedSprite.play("Saut2")
		elif ScriptStats.forme == 1 :
			$AnimatedSprite.play("Saut1")


func ramassageArgent():
	$AudioStreamPlayer2D.stream = son_argent
	$AudioStreamPlayer2D.play()


func mort():
	$AnimatedSprite.play("Mort")
	$TempsDuMort.start()
	$AudioStreamPlayer2D.stream = son_mort
	$AudioStreamPlayer2D.play()

	yield($TempsDuMort, "timeout")
	$AnimatedSprite.play("Mort")

	set_position(ScriptStats.positionDepart)
	ScriptStats.vie -= 1
	ScriptGUI.change_val_vie(ScriptStats.vie)
	ScriptStats.forme = 3
	ScriptGUI.etat_du_joueur(ScriptStats.forme)
	#ScriptStats.cle = 0
	#ScriptGUI.change_val_cle(ScriptStats.cle)

	
	
	

	
