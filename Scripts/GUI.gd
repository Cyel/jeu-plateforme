extends CanvasLayer

onready var ScriptJoueur = get_tree().get_root().find_node("Joueur", true, false)
onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)

func _ready():
	if (ScriptStats.niveau <= 1 ):
		ScriptStats.niveau=1
		ScriptStats.argent=5
		ScriptStats.argentPourVie=0
		ScriptStats.vie=0
		ScriptStats.forme=3
		ScriptStats.cle=0

	$Argent/LabelArgent.text = str(ScriptStats.argent)
	$Cle/LabelCle.text = str(ScriptStats.cle)
	$Vie/LabelVie.text = str(ScriptStats.vie)
	$Vie/AnimationFormeJoueur.play("PleineForme")
	$Niveau/LabelNiveau.text = str(ScriptStats.niveau)
	

func change_val_argent(argent):
	$Argent/LabelArgent.text = str(argent)
	
func change_val_cle(cle):
	$Cle/LabelCle.text = str(cle)
	
func change_val_vie(vie):
	$Vie/LabelVie.text = str(vie)
	
func change_niveau(level):
	$Niveau/LabelNiveau.text = str(level)
	
func etat_du_joueur(_forme):
	if ScriptStats.forme == 3 :
		$Vie/AnimationFormeJoueur.play("PleineForme")
	elif ScriptStats.forme == 2 :
		$Vie/AnimationFormeJoueur.play("Forme")
	elif ScriptStats.forme == 1 :
		$Vie/AnimationFormeJoueur.play("Fatigue")
	elif ScriptStats.forme <= 0 :
		ScriptJoueur.mort()
	else :
		get_tree().change_scene("res://Scenes/Menu.tscn")
		print ("Erreur Animation de la forme du joueur")
		
	if ScriptStats.vie < 0 : 
		ScriptJoueur.mort()
		get_tree().reload_current_scene()
		ScriptStats.vie=0



func _ChangementDeScene():
	ScriptStats.niveau += 1
	if ScriptStats.niveau < 4 :
		get_tree().change_scene("res://Scenes/Level"+str(ScriptStats.niveau)+".tscn")
	else :
		get_tree().change_scene("res://Scenes/Menu.tscn")
	ScriptStats.cle = 0
	change_val_cle(ScriptStats.cle)
	ScriptStats.forme = 3
	etat_du_joueur(ScriptStats.forme)


