extends StaticBody2D

onready var ScriptGUI = get_tree().get_root().find_node("GUI", true, false)
onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)

func _on_Area2D_body_entered(body):
	if body.is_in_group("Joueur"):
		if ScriptStats.forme > 0 :
			ScriptStats.forme -= 1
		ScriptGUI.etat_du_joueur(ScriptStats.forme)

