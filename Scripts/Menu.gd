extends CanvasLayer

onready var ScriptStats = get_tree().get_root().find_node("StatsJeu", true, false)

func _on_BoutonJouer_pressed():
	get_tree().change_scene("res://Scenes/Level1.tscn")
	ScriptStats.niveau = 1

func _process(_delta): 
	if (Input.is_action_pressed("jump")):
		get_tree().change_scene("res://Scenes/Level1.tscn")
		ScriptStats.niveau = 1
	
func _on_BoutonQuitter_pressed():
	get_tree().quit()
	
func _ready():
	ScriptStats.niveau = 1

#A faire : 
#-Animation de changement de niveau (et clés qui se remettent bienà zéro si plus de 3 clés dans le niveau). 
#-Sauvegarde du jeu (et des paramètres de StatsJeu)
#-But du jeu et décors
