Un jeu de plateforme en 2d, développé à l’aide de l'éditeur de jeux vidéo [Gogot](https://godotengine.org/).

Le jeu est composé de trois niveaux, pour passer au suivant, vous devez récupérer l’ensemble des clés dispersées sur le niveau.
Lorsque vous touchez un monstre, vous êtes blessé, la troisième blessure vous est fatale. Cependant, les joyaux vous permettent de résorber vos blessures.

![Illustration du Jeu de plateforme](https://fccl-vandoeuvre.fr/divers/projets/jeux_video/jeu_plateforme/v0.1/illustration.png)

Le jeu est actuellement en développement, [des versions compilées](https://fccl-vandoeuvre.fr/divers/projets/jeux_video/jeu_plateforme/) sont proposées pour le découvrir.